#
# Deploys a site using ansible.
#

FROM alpine:latest
MAINTAINER Jarno Antikainen <jarno.antikainen@wysiwyg.fi>

LABEL Description="Deploys a site" Vendor="Wysiwyg Oy"

RUN apk add --no-cache ansible bash curl openssh-client python rsync

# Ansible
COPY roles /etc/ansible/roles
COPY ansible.cfg /etc/ansible/ansible.cfg

CMD [ "ansible-playbook", "/project/deployment/deploy.yml" ]
